/* --COPYRIGHT--,BSD
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * --/COPYRIGHT--*/
//******************************************************************************
//   2020-05-20 / Steddin
//!  ACLK = n/a, MCLK = SMCLK = default DCO
//!  S1 with internal pullup resistor
//!
//!           MSP430FR2xx_4xx Board
//!            -----------------
//!        /|\|                 |
//!         | |                 |
//!         --|RST              |
//!           |                 |
//!      S1-->| P4.1            |-->LED1 red (P1.0)
//!   pullup  |                 |
//!
//!
//!
//! Program is derived from TI driverlib sample code project gpio_ex3_softwarePoll
//! and project cs_ex3_XT1SourcesACLK
//! Modified: Sven Steddin (2020-05-19)
//******************************************************************************
/*
 * ESR-Projekt Alarmanlage
 * Gruppe 11
 * Mohammed Abdulwahab Kalash
 * Patrik Keppeler
 * Kai Stahl
 *
 * Zweckbestimmung:
 * Alarmanlage
 * TBD
 *
 * Bestimmungsgem��er Gebrauch:
 * TBD
 */
#include <driverlib.h>
#include <msp430.h>
#include "Board.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

static Timer_B_initUpModeParam param = { 0 };
static Timer_B_initUpModeParam param1 = { 1 };

//  GUI Composer Variablen
bool lautsprecher_status = false;   // Lautsprecher
bool LED_RED_Status = false;        // Rote LED
bool LED_GREEN_Status = false;      // Gr�ne LED
bool Bewegungsmelder_Status = false;// Bewegungsmelder

//  Spalten des Numpads
#define COL0 (P3IN & BIT0)  // "A", "B", "C", "D"
#define COL1 (P3IN & BIT1)  // "3", "6", "9", "#"
#define COL2 (P3IN & BIT2)  // "2", "5", "8", "0"
#define COL3 (P3IN & BIT3)  // "1", "4", "7", "*"
#define PRESS 0

int codeEingabe = 0;    // Code der �ber das Numpad eingegeben wurde
int passwort = 141009;  // Festgelegtes Passwort

//Functions

char getKey(void);
void delay_ms(unsigned int ms);

void main(void)
{
    //Stop watchdog timer
    WDT_A_hold(WDT_A_BASE);

    // Configure Pins for XIN and XOUT
    //Set P2.6 and P2.7 as Module Function Input.
    GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P2, GPIO_PIN6 + GPIO_PIN7, GPIO_SECONDARY_MODULE_FUNCTION);

    // Bewegungsmelder als Input
    GPIO_setAsInputPin(GPIO_PORT_P1, GPIO_PIN5);
    // Interrupt f�r Bewegungssensor
    GPIO_selectInterruptEdge(GPIO_PORT_P1, GPIO_PIN5, GPIO_LOW_TO_HIGH_TRANSITION);
    GPIO_clearInterrupt(GPIO_PORT_P1,GPIO_PIN5);
    GPIO_enableInterrupt(GPIO_PORT_P1, GPIO_PIN5);

    // Lautsprecher als Output
    GPIO_setAsOutputPin(GPIO_PORT_P1, GPIO_PIN4);

    //Set LED1 (red) to output direction
    GPIO_setAsOutputPin(GPIO_PORT_LED1, GPIO_PIN_LED1);
    GPIO_setOutputLowOnPin(GPIO_PORT_LED1, GPIO_PIN_LED1);

    //Set LED2 (green) to output direction
    GPIO_setAsOutputPin(GPIO_PORT_LED2, GPIO_PIN_LED2);
    GPIO_setOutputHighOnPin(GPIO_PORT_LED2, GPIO_PIN_LED2);
    LED_GREEN_Status = true;

    //Set S1 to input direction with internal pull-up resistance
    GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_S1, GPIO_PIN_S1);
    GPIO_selectInterruptEdge(GPIO_PORT_S1, GPIO_PIN_S1, GPIO_HIGH_TO_LOW_TRANSITION);
    GPIO_clearInterrupt(GPIO_PORT_S1, GPIO_PIN_S1);
    GPIO_enableInterrupt(GPIO_PORT_S1, GPIO_PIN_S1);

    //Set S2 to input direction with internal pull-up resistance
    GPIO_setAsInputPinWithPullUpResistor(GPIO_PORT_S2, GPIO_PIN_S2);
    GPIO_selectInterruptEdge(GPIO_PORT_S2, GPIO_PIN_S2, GPIO_HIGH_TO_LOW_TRANSITION);
    GPIO_clearInterrupt(GPIO_PORT_S2, GPIO_PIN_S2);
    GPIO_enableInterrupt(GPIO_PORT_S2, GPIO_PIN_S2);

    // Numpad Port Initialisierung
    GPIO_selectInterruptEdge(GPIO_PORT_P3, GPIO_PIN0, GPIO_LOW_TO_HIGH_TRANSITION);
    GPIO_selectInterruptEdge(GPIO_PORT_P3, GPIO_PIN1, GPIO_LOW_TO_HIGH_TRANSITION);
    GPIO_selectInterruptEdge(GPIO_PORT_P3, GPIO_PIN2, GPIO_LOW_TO_HIGH_TRANSITION);
    GPIO_selectInterruptEdge(GPIO_PORT_P3, GPIO_PIN3, GPIO_LOW_TO_HIGH_TRANSITION);
    GPIO_selectInterruptEdge(GPIO_PORT_P3, GPIO_PIN4, GPIO_LOW_TO_HIGH_TRANSITION);
    GPIO_selectInterruptEdge(GPIO_PORT_P3, GPIO_PIN5, GPIO_LOW_TO_HIGH_TRANSITION);
    GPIO_selectInterruptEdge(GPIO_PORT_P3, GPIO_PIN6, GPIO_LOW_TO_HIGH_TRANSITION);
    GPIO_selectInterruptEdge(GPIO_PORT_P3, GPIO_PIN7, GPIO_LOW_TO_HIGH_TRANSITION);
    // Numpad clear Interrupts
    GPIO_clearInterrupt(GPIO_PORT_P3, GPIO_PIN1);
    GPIO_clearInterrupt(GPIO_PORT_P3, GPIO_PIN2);
    GPIO_clearInterrupt(GPIO_PORT_P3, GPIO_PIN3);
    GPIO_clearInterrupt(GPIO_PORT_P3, GPIO_PIN4);
    GPIO_clearInterrupt(GPIO_PORT_P3, GPIO_PIN5);
    GPIO_clearInterrupt(GPIO_PORT_P3, GPIO_PIN6);
    GPIO_clearInterrupt(GPIO_PORT_P3, GPIO_PIN7);
    // Numpad enable Interrupts
    GPIO_enableInterrupt(GPIO_PORT_P3, GPIO_PIN1);
    GPIO_enableInterrupt(GPIO_PORT_P3, GPIO_PIN2);
    GPIO_enableInterrupt(GPIO_PORT_P3, GPIO_PIN3);
    GPIO_enableInterrupt(GPIO_PORT_P3,GPIO_PIN4);
    GPIO_enableInterrupt(GPIO_PORT_P3, GPIO_PIN5);
    GPIO_enableInterrupt(GPIO_PORT_P3, GPIO_PIN6);
    GPIO_enableInterrupt(GPIO_PORT_P3, GPIO_PIN7);

    // LowPowerMode5
    PMM_unlockLPM5();

    //Initializes the XT1 crystal oscillator with no timeout
    //In case of failure, code hangs here.
    //For time-out instead of code hang use CS_turnOnXT1LFWithTimeout()
    CS_turnOnXT1LF(CS_XT1_DRIVE_0);

    CS_initClockSignal(CS_ACLK, CS_XT1CLK_SELECT, CS_CLOCK_DIVIDER_1);

    //clear all OSC fault flag
    CS_clearAllOscFlagsWithTimeout(1000);

    //Enable oscillator fault interrupt
    SFR_enableInterrupt(SFR_OSCILLATOR_FAULT_INTERRUPT);

    __bis_SR_register(GIE + LPM0_bits);         // Global interrupt enable
    __no_operation();                           // debugger breakpoint location

}

// TimerB0 Interrupt Vector (TBxIV) handler
/**
 * @brief ISR f�r Port2
 *
 * ISR wird aufgerufen, sobald die f�r den Timer beim Dr�cken von S1
 * oder Aktivierung durch den Bewegungssensor eingestellte Zeit abgelaufen ist.
 */
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=TIMER0_B0_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(TIMER0_B0_VECTOR)))
#endif
void TIMER0_B0_ISR(void)
{
    // after timer period is finished:
    // toggle LEDs back to initial state
    GPIO_setOutputHighOnPin(GPIO_PORT_LED1, GPIO_PIN_LED1); // Rote LED an
    GPIO_setOutputLowOnPin(GPIO_PORT_LED2, GPIO_PIN_LED2);  // Gr�ne LED aus
    LED_GREEN_Status = false;   // GUI-Composer Variablen
    LED_RED_Status = true;      // GUI-Composer Variablen
    Bewegungsmelder_Status = false; // GUI-Composer Variablen

    // Aktivierung des Lautsprechers
    lautsprecher_status = true;     // GUI-Composer Variablen
    while (lautsprecher_status)
    {
        GPIO_toggleOutputOnPin(GPIO_PORT_P1, GPIO_PIN4);    // Lautsprecher Signal
    }

    Timer_B_stop(TIMER_B0_BASE);    // stop TimerB
}

/**
 * @brief ISR f�r Port4
 *
 * ISR wird aufgerufen, sobald an P4.1 eine fallende Flanke auftritt. Es wird
 * der Timer TB0 so eingestellt, das dieser 1s nach loslassen von S1 einen Interrupt ausl�st,
 * um die LED wieder in ihren Ausgangszustand zu bringen.
 */
#pragma vector=PORT4_VECTOR
__interrupt void Port_4(void)
{
    // test button press or release
    if ((P4IES & BIT1) == BIT1)
    {
        // on high to low transition:
        // activate low to high transition: on button release
        P4IES &= ~BIT1;

        // TimerB
        param.clockSource = TIMER_B_CLOCKSOURCE_ACLK;
        param.clockSourceDivider = TIMER_B_CLOCKSOURCE_DIVIDER_4; // Zeit wird auf 4sec gesetzt
        param.timerPeriod = 0x7FFF;     // wenn 2^15 Taktimpulse gez�hlt
                                        // wurden, erfolgt ein Interrupt
                                        // Periodendauer somit 1s bei Verwendung von AClk,
                                        // bzw. 4s, wenn AClk noch durch 4 geteilt wird
        param.timerInterruptEnable_TBIE =TIMER_B_TBIE_INTERRUPT_DISABLE;         // no interrupt on 0x0000
        param.captureCompareInterruptEnable_CCR0_CCIE = TIMER_B_CAPTURECOMPARE_INTERRUPT_ENABLE;    // interrupt on TRmax
        param.timerClear = TIMER_B_DO_CLEAR;
        param.startTimer = false;

        // start Timer:
        Timer_B_initUpMode(TIMER_B0_BASE, &param);
    }
    else
    {
        // on low to high transistion:
        // activate high to low transition: on button press
        P4IES |= BIT1;
        // start timer period
        Timer_B_startCounter(TIMER_B0_BASE, TIMER_B_UP_MODE);
        // activate red LED
        GPIO_setOutputHighOnPin(GPIO_PORT_LED1, GPIO_PIN_LED1); // Rote LED an
        GPIO_setOutputHighOnPin(GPIO_PORT_LED2, GPIO_PIN_LED2); // Gr�ne LED an
    }

    // Ein Port Interrupt wird nicht automatisch zur�ckgesetzt; das
    // Interruptflag des Pins, welches den Interrupt ausgel�st hat, muss
    // im Code zur�ckgesetzt werden, damit der Interrupt nicht gleich wieder
    // ausgel�st wird.
    GPIO_clearInterrupt(GPIO_PORT_S1, GPIO_PIN_S1);
}

// �bereinstimmung des eingegebenen Codes mit dem Code in dem initialisierten Array?
// Wenn Ja --> stop den Counter, oder lass den Timer Laufen
// Port 2 interrupt service routine
#pragma vector=PORT2_VECTOR
__interrupt void Port_2(void)
{
    GPIO_setOutputLowOnPin(GPIO_PORT_LED1, GPIO_PIN_LED1);  // Rote LED aus
    GPIO_setOutputHighOnPin(GPIO_PORT_LED2, GPIO_PIN_LED2); // Gr�ne LED an
    LED_RED_Status = false; // GUI-Composer Variablen
    LED_GREEN_Status = true;    // GUI-Composer Variablen
    Bewegungsmelder_Status = false; // GUI-Composer Variablen

    Timer_B_stop(TIMER_B0_BASE);    // stop TimerB

    // Ein Port Interrupt wird nicht automatisch zur�ckgesetzt; das
    // Interruptflag des Pins, welches den Interrupt ausgel�st hat, muss
    // im Code zur�ckgesetzt werden, damit der Interrupt nicht gleich wieder
    // ausgel�st wird.
    GPIO_clearInterrupt(GPIO_PORT_S2, GPIO_PIN_S2);
}

#pragma vector=PORT1_VECTOR
__interrupt void Port_1(void)
{
    // Implementierung des Bewegungsmelders
    // TimerB
    param.clockSource = TIMER_B_CLOCKSOURCE_ACLK;
    param.clockSourceDivider = TIMER_B_CLOCKSOURCE_DIVIDER_10;
    param.timerPeriod = 0x7FFF;     // wenn 2^15 Taktimpulse gez�hlt
                                    // wurden, erfolgt ein Interrupt
                                    // Periodendauer somit 1s bei Verwendung von AClk,
                                    // bzw. 4s, wenn AClk noch durch 4 geteilt wird
    param.timerInterruptEnable_TBIE =
    TIMER_B_TBIE_INTERRUPT_DISABLE;         // no interrupt on 0x0000
    param.captureCompareInterruptEnable_CCR0_CCIE =
    TIMER_B_CAPTURECOMPARE_INTERRUPT_ENABLE;    // interrupt on TRmax
    param.timerClear = TIMER_B_DO_CLEAR;
    param.startTimer = false;

    // start Timer:
    Timer_B_initUpMode(TIMER_B0_BASE, &param);
    Timer_B_startCounter(TIMER_B0_BASE, TIMER_B_UP_MODE);

    GPIO_setOutputHighOnPin(GPIO_PORT_LED1, GPIO_PIN_LED1); // Rote LED an
    GPIO_setOutputHighOnPin(GPIO_PORT_LED2, GPIO_PIN_LED2); // Gr�ne LED an
    LED_RED_Status = true;  // GUI-Composer Variablen
    LED_GREEN_Status = true;    // GUI-Composer Variablen
    Bewegungsmelder_Status = true;  // GUI-Composer Variablen

    // Ein Port Interrupt wird nicht automatisch zur�ckgesetzt; das
    // Interruptflag des Pins, welches den Interrupt ausgel�st hat, muss
    // im Code zur�ckgesetzt werden, damit der Interrupt nicht gleich wieder
    // ausgel�st wird.
    GPIO_clearInterrupt(GPIO_PORT_P1, GPIO_PIN5);
}

// Numpad Zuweisungen:
//1. Reihe: P3.7 | "1" = P3.3, "2" = P3.2, "3" = P3.1, "A" = P3.0
//2. Reihe: P3.6 | "4" = P3.3, "5" = P3.2, "6" = P3.1, "B" = P3.0
//3. Reihe: P3.5 | "7" = P3.3, "8" = P3.2, "9" = P3.1, "C" = P3.0
//4. Reihe: P3.4 | "*" = P3.3, "0" = P3.2, "#" = P3.1, "D" = P3.0
#pragma vector=PORT3_VECTOR
__interrupt void Port_3(void)
{
    // Implementierung des Numpads
    P3SEL0 &= 0x00;
    P3DIR = 0xF0;
    P3OUT &= 0x00;
    P3REN |= 0x0F; //Enable resistor
    P3OUT |= 0xFF; //Set resistor to pull-up

//Scan Row 0
    P3OUT &= (~BIT4); //set P3.4 low
    if (COL0 == PRESS)
    {
        codeEingabe = 14;
        //D
    }

    if (COL1 == PRESS)
    {
        if (codeEingabe == 14)
        {
            codeEingabe = 1410;
        }
        //#
    }

    if (COL2 == PRESS)
    {
        if (codeEingabe = 1410)
        {
            codeEingabe = 14100;
        }
        //0
    }

    if (COL3 == PRESS)
    {
        codeEingabe = codeEingabe + 15;
        //*
    }
    P3OUT |= (BIT4);

//Scan Row 1
    P3OUT &= (~BIT5); //set P3.5 low
    if (COL0 == PRESS)
    {
        codeEingabe = codeEingabe + 12;
        //C
    }

    if (COL1 == PRESS)
    {
        if (codeEingabe = 14100)
        {
            codeEingabe = 141009;

            if (codeEingabe == passwort)
            {
                GPIO_setOutputHighOnPin(GPIO_PORT_LED2, GPIO_PIN_LED2);
                GPIO_setOutputLowOnPin(GPIO_PORT_LED1, GPIO_PIN_LED1);
                Timer_B_stop(TIMER_B0_BASE);

                GPIO_clearInterrupt(GPIO_PORT_P3, GPIO_PIN1);
                GPIO_clearInterrupt(GPIO_PORT_P3, GPIO_PIN2);
                GPIO_clearInterrupt(GPIO_PORT_P3, GPIO_PIN3);
                GPIO_clearInterrupt(GPIO_PORT_P3, GPIO_PIN4);
                GPIO_clearInterrupt(GPIO_PORT_P3, GPIO_PIN5);
                GPIO_clearInterrupt(GPIO_PORT_P3, GPIO_PIN6);
                GPIO_clearInterrupt(GPIO_PORT_P3, GPIO_PIN7);
                codeEingabe = 0;
            }
        }
        //9
    }

    if (COL2 == PRESS)
    {
        codeEingabe = codeEingabe + 8;
        //8
    }

    if (COL3 == PRESS)
    {
        codeEingabe = codeEingabe + 7;
        //7
    }
    P3OUT |= (BIT5);

//Scan Row 2
    P3OUT &= (~BIT6); //set P3.6 low
    if (COL0 == PRESS)
    {
        codeEingabe = codeEingabe + 11;
        //B
    }

    if (COL1 == PRESS)
    {
        codeEingabe = codeEingabe + 6;
        //6
    }

    if (COL2 == PRESS)
    {
        codeEingabe = codeEingabe + 5;
        //5
    }

    if (COL3 == PRESS)
    {
        codeEingabe = codeEingabe + 4;
        //4
    }
    P3OUT |= (BIT6);

//Scan Row 3
    P3OUT &= (~BIT7); //set P3.7 low
    if (COL0 == PRESS)
    {
        codeEingabe = codeEingabe + 10;
        //A
    }

    if (COL1 == PRESS)
    {
        codeEingabe = codeEingabe + 3;
        //3
    }

    if (COL2 == PRESS)
    {
        codeEingabe = codeEingabe + 2;
        //2
    }

    if (COL3 == PRESS)
    {
        codeEingabe = codeEingabe + 1;
        //1
    }
    P3OUT |= (BIT7);

    //NULL

}

//  Delay
void delay_ms(unsigned int ms)
{
    while (ms)
    {
        __delay_cycles(1000); //1000 for 1MHz (default clock setting)
        ms--;
    }
}

